export const TYPES = {
	Bot: Symbol("Bot"),
	Client: Symbol("Client"),
	Token: Symbol("Token"),
};

export type regexpType = { match: RegExpMatchArray };
